.DEFAULT_GOAL := build
.PHONY: build clean

TITLE := $(shell cat 'id/title.txt')
LANGUAGE := $(shell cat 'id/language.txt')
EDITIONUID := $(shell cat 'id/editionuid.txt')

docs/20201122.building-a-community-as-a-service.thomas-debesse.pdf: docs/20201122.building-a-community-as-a-service.thomas-debesse.odp
	soffice --headless --convert-to pdf --outdir docs docs/20201122.building-a-community-as-a-service.thomas-debesse.odp

slides/slide-001.png: docs/20201122.building-a-community-as-a-service.thomas-debesse.pdf
	mkdir -p slides
	scripts/pdftopng docs/20201122.building-a-community-as-a-service.thomas-debesse.pdf slides/slide

live/intro.png: live/live.webm
	ffmpeg -y -i live/live.webm -ss 00:00:03 -frames:v 1 $@

live/video.webm: live/live.webm
	ffmpeg -y -i live/live.webm -r 25 -an -vcodec copy $@

live/audio.opus: live/live.webm
	ffmpeg -y -i live/live.webm -acodec copy -ac 1 -vn $@

render/video.webm: slides/slide-001.png clips/camera-left.webm clips/camera-right.webm live/video.webm live/intro.png
	mkdir -p render
	kdenlive_render melt render/video.mlt $@

render/audio.mka: clips/microphone-center.flac live/audio.opus
	mkdir -p render
	kdenlive_render melt render/audio.mlt $@

render/audio.opus: render/audio.mka
	ffmpeg -y -i render/audio.mka -b:a 192000 $@

subtitles/%.vtt: subtitles/%.srt
	srt-vtt $<

chapters/chapters.xml: chapters/eng.txt
	scripts/mkctxttoxml chapters/chapters.xml \
		$(EDITIONUID) \
		$(LANGUAGE) \
		live/live.webm \
		chapters/*.txt

metadata/matroskatags.xml: metadata/matroskatags.in.xml metadata/matroskatags.dtd
	sed -e 's/\[EDITIONUID\]/'"$(EDITIONUID)"'/;s/\[TITLE\]/'"$(TITLE)"'/' \
		metadata/matroskatags.in.xml > metadata/matroskatags.xml
	xmllint metadata/matroskatags.xml \
	--noout \
	--dtdvalid metadata/matroskatags.dtd

metadata/cover.jpg: slides/slide-001.png
	convert -quality 97 $< $@

20201122.building-a-community-as-a-service.thomas-debesse.mkv: render/video.webm render/audio.opus metadata/matroskatags.xml chapters/chapters.xml metadata/matroskatags.xml metadata/cover.jpg subtitles/eng.srt subtitles/fre.srt
	mkvmerge \
		--output $@ \
		--title '$(TITLE)' \
		--default-language '$(LANGUAGE)' \
		--global-tags metadata/matroskatags.xml \
		--chapter-charset UTF-8 \
		--chapters chapters/chapters.xml \
		--attachment-mime-type image/jpeg \
		--attach-file metadata/cover.jpg \
		--language 0:eng \
		render/video.webm \
		--language 0:eng \
		render/audio.opus \
		--language 0:eng \
		subtitles/eng.srt \
		--language 0:fre \
		subtitles/fre.srt

build: 20201122.building-a-community-as-a-service.thomas-debesse.mkv subtitles/eng.vtt subtitles/fre.vtt

clean:
	rm live/intro.png
	rm live/audio.opus
	rm live/video.webm
	rm 20201122.building-a-community-as-a-service.thomas-debesse.mkv
