Building a community as a service
=================================

From November 19th to 22nd, 2020, Debian organized an online “MiniDebConf” with video game as main theme, and nattie invited the Unvanquished project to submit a talk. As an active member of this project I proposed the following topic:

> Building a community as a service: how to stop suffering from “this code is meant to be forked”.

And here is the presentation:

> Earlier id Tech engines were well known to have seen their source code opened when they were replaced and thus unprofitable. While this was a huge benefit for mankind, game developers still suffer today from design choices and mindset induced by the fact such code base was meant to die. 20 years later we will focus on id Tech 3 heritage, how both the market, open source communities and development practices evolved, and embark in the journey of the required transition from dead code dump to an ecosystem as a service.

Starting with various examples from the video game industry, the conference is the result of fifteen years of observation and immersion, and is developing a broader reflection on the nature of a service, the need to develop communities, the place of collaboration in an open source community, how design choices can induce a mindset that feeds design in turn, etc. Some issues are addressed such as the (possibly hidden) cost of certain practices, the nature of an economy, or how certain methods more readily encourage the production of waste or else the recycling of production.

This 45-minute conference was given online, in English, on Sunday, November 22 at 19:30 UTC.


Disclaimer
----------

This conference topic chooses to study the legacy of id Software’s liberated code from a perspective that has been neglected by studies: what can be improved. id Software’s legacy has deeply changed the landscape, code liberation has fueled both academia and a lively passion in many people, and our Unvanquished game itself would simply not exist without this legacy. Thus, nothing should be taken away from the gratitude to the various actors who have made this possible. However, beyond focusing on what is very good, it seems interesting to also study what is less good, because it is often less success than failure that makes it possible to learn and surpass oneself.

This talk features personal opinions and makes use of colorful (if not doom-esque) metaphors.


Watching the conference
-----------------------

The conference can be seen on [Youtube](https://www.youtube.com/watch?v=ijfuuCGOTCA) with English and French subtitles, and also on [the Debian Peertube](https://peertube.debian.social/videos/watch/9063a45f-041e-4e48-a7c6-9dd31be49c1c) (no subtitles yet):

[![Video previsualisation](extra/video-preview.mini.png)](https://www.youtube.com/watch?v=ijfuuCGOTCA)

The video can be [downloaded there](https://dl.illwieckz.net/b/conference/20201122.debian-minidebconf-online/20201122.building-a-community-as-a-service.thomas-debesse.mkv) (including subtitles and chapters in both English and French language). Slides (in English) can be [downloaded there](https://dl.illwieckz.net/b/conference/20201122.debian-minidebconf-online/20201122.building-a-community-as-a-service.thomas-debesse.pdf).


Published transcripts
---------------------

- [Building a community as a service](https://unvanquished.net/building-a-community-as-a-service/) (`unvanquished.net`, English)
- [Bâtir une communauté comme un service](https://linuxfr.org/news/batir-une-communaute-comme-un-service) (`linuxfr.org`, French).


About this repository
---------------------

This repository provides transcripts, chapters, subtitles and sources for the slides and for the video itself (audio and video rushes had been pre-edited for restraint reason, but the final cut is expected to be reproduceable).

Transcripts, chapters and subtitles are both available in English and French language.

Thanks to Debian for hosting and organizing the event and to Thomas Vincent from Debian France for the transformation of the transcript into subtitles and the tedious work of synchronization, as well as his meticulous work of proofreading and correcting the French translation.


Licensing
---------

All original content I (Thomas Debesse, also known as _illwieckz_) produced for this conference (voice/picture/text…) is distributed under [CC0 1.0 licence](https://creativecommons.org/publicdomain/zero/1.0). Other licenses may apply (logos, decorative images, the live part).